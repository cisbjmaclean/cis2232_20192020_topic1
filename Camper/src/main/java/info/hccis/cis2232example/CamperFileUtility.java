package info.hccis.cis2232example;

import com.google.gson.Gson;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.RandomAccessFile;

/**
 * This class will contain methods associated with writing campers to a file.
 *
 * @author bjmaclean
 * @since 20180921
 */
public class CamperFileUtility {

    private String fileName;

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public CamperFileUtility(String fileName) {
        this.fileName = fileName;
    }

    /**
     * This method will write a camper to a random access file. The camper's
     * registration id is used to determine the location in the file and Gson is
     * used to encode the camper as json.
     *
     * @param raf random acccess file
     * @param camper camper to write
     * @throws IOException
     * @since 20180921
     * @author BJM
     */
    public static void writeCamperToRandomFile(RandomAccessFile raf, Camper camper) throws IOException {

        raf.seek((camper.getRegistrationId() - 1) * 100); //Seek to the correctly location
        
        raf.writeBytes(camper.getJsonMaxLength());  //Write the json

    }

    /**
     * From:
     * https://examples.javacodegeeks.com/core-java/io/fileoutputstream/how-to-write-an-object-to-file-in-java/
     * Write an object to a file.
     *
     * @param serObj Serializable object
     * @since 20180924
     * @author BJM
     */
    public void WriteObjectToFile(Object serObj) {

        try {

            FileOutputStream fileOut = new FileOutputStream(fileName);
            ObjectOutputStream objectOut = new ObjectOutputStream(fileOut);
            objectOut.writeObject(serObj);
            objectOut.close();

            System.out.println("The Object  was succesfully written to a file");

        } catch (Exception ex) {

            ex.printStackTrace();

        }

    }

    /**
     * From:
     * http://www.avajava.com/tutorials/lessons/how-do-i-write-an-object-to-a-file-and-read-it-back.html
     * Read an object to a file.
     *
     * @param serObj Serializable object
     * @since 20180924
     * @author BJM
     */
    public Object readObjectFromFile() {

        try {

// read object from file
            FileInputStream fis = new FileInputStream(fileName);
            ObjectInputStream ois = new ObjectInputStream(fis);
            Object result = ois.readObject();
            ois.close();
            return result;
            
        } catch (Exception ex) {

            ex.printStackTrace();
            return null;
        }

    }

}
