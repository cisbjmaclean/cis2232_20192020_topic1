package info.hccis.cis2232example;

import com.google.gson.Gson;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Scanner;

/**
 * This class will represent a camper.
 *
 * @author bjmaclean
 * @since 20150915
 */
public class Camper implements Serializable {

    public static final int RECORD_LENGTH = 100; //max record length
    private static int maxRegistrationId;
    private int registrationId;
    private String firstName;
    private String lastName;
    private String dob;

    public Camper() {
        //do nothing 
    }

    /**
     * Default constructor which will get info from user
     *
     * @since 20150917
     * @author BJ MacLean
     * 
     * Changes
     * Issue#4 Added validation BJM 20190923
     */
    public Camper(boolean getFromUser) {
        if (getFromUser) {

            /*
           BJM 20180921 RANDOM ACCESS CHANGES 
           Asking the user for registration id.  This will be used
           by the program to update the correct location in the random access json file.
             */
            do {
                System.out.println("Registration id");
                this.registrationId = FileUtility.getInput().nextInt();
                FileUtility.getInput().nextLine();
            } while (registrationId < 1 || registrationId > 100);

            do {
                System.out.println("Enter first name");
                this.firstName = FileUtility.getInput().nextLine();
            } while (firstName.length() < 1);

            do {
                System.out.println("Enter last name");
                this.lastName = FileUtility.getInput().nextLine();
            } while (lastName.length() < 1);

            do {
                System.out.println("Enter dob");
                this.dob = FileUtility.getInput().nextLine();
                //https://www.baeldung.com/java-date-regular-expressions
            } while (!this.dob.matches("^((19|2[0-9])[0-9]{2})-(0[1-9]|1[012])-(0[1-9]|[12][0-9]|3[01])$"));

        }
    }

    /**
     * Custom constructor with all info
     *
     * @param registrationId
     * @param firstName
     * @param lastName
     * @param dob
     *
     * @author BJ MacLean
     * @since 20150917
     */
    public Camper(int registrationId, String firstName, String lastName, String dob) {
        this.registrationId = registrationId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.dob = dob;
    }

    /**
     * constructor which will create from String array
     *
     * @since 20150917
     * @author BJ MacLean
     */
    public Camper(String[] parts) {
        this(Integer.parseInt(parts[0]), parts[1], parts[2], parts[3]);
        /*
         This makes sure that we capture/set the maximum registration id as we load
         all of the entries from the file.  Then when we add a new camper it will
         use this to set the next registration id.
         */
        if (Integer.parseInt(parts[0]) > maxRegistrationId) {
            maxRegistrationId = Integer.parseInt(parts[0]);
        }
    }

    /**
     * constructor which will create from String array
     *
     * @since 20150917
     * @author BJ MacLean
     */
    public Camper(String csvValues) {
        this(csvValues.split(","));
    }

    /**
     * give back a new instance based on the json string passed in.
     *
     * @since 20190918
     * @author BJ MacLean
     */
    public static Camper newInstance(String jsonIn) {
        Gson gson = new Gson();
        return gson.fromJson(jsonIn, Camper.class);
    }

    public String getCSV() {
        return registrationId + "," + firstName + "," + lastName + "," + dob;
    }

    /**
     * This will give back the Camper details
     *
     * @param withLineFeed Return with a line feed character
     * @return camper details
     * @since 20170915
     * @author cis2232
     */
    public String getCSV(boolean withLineFeed) {
        if (withLineFeed) {
            return getCSV() + System.lineSeparator();
        } else {
            return getCSV();
        }
    }

    public String getJson() {
        Gson gson = new Gson();
        return gson.toJson(this);
    }

    /**
     * Return json with padding spaces.
     *
     * @return json
     * @since 20190920
     * @author BJM
     */
    public String getJsonMaxLength() {
        //BJM 20180924 //Making json 100 length
        String camperJson = getJson();
        while (camperJson.length() < Camper.RECORD_LENGTH) {
            camperJson += " ";
        }
        return camperJson;
    }

    public int getRegistrationId() {
        return registrationId;
    }

    public void setRegistrationId(int registrationId) {
        this.registrationId = registrationId;
    }

    public static int getMaxRegistrationId() {
        return maxRegistrationId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    /**
     * Show a camper Changes: Issue#2 formatting output for a camper
     *
     * @author AC
     * @since 20190923
     * @return
     */
    @Override
    public String toString() {
        int age = 0;
        if (dob != null) {
            String[] dateOfBirth = dob.split("/");
            for (String current : dateOfBirth) {
                if (current.length() == 4) {
                    int yearBorn = Integer.parseInt(current);
                    age = (Calendar.getInstance().get(Calendar.YEAR) - yearBorn);
                }
            }
        }

        return "Camper #" + registrationId + " " + firstName + " " + lastName + " Age=" + age;
    }

    public void display() {
        System.out.println(this.toString());
    }

}
