package info.hccis.cis2232example;

import com.google.gson.Gson;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author bjmaclean
 */
public class MainClass {

    public static String MENU = "Menu Options:\nA) Add/Update camper\nS) Show campers\nX) Exit";
    public static final String PATH = "/cis2232_20192020/";
    public static String FILE_NAME = PATH + "campersRandom.json";

    public static void main(String[] args) throws IOException {
        //Create a file
        Files.createDirectories(Paths.get(PATH));

        //Test writing an object to file.
        testWriteObject();

        RandomAccessFile raf = null;
        raf = setupRandomAccessFile(raf);

        ArrayList<Camper> theList = new ArrayList();

        //BJM 20190920 load from the random access file.
        loadCampers(theList, raf);

        String option;
        do {
            System.out.println(MENU);
            option = FileUtility.getInput().nextLine().toUpperCase();

            switch (option) {
                case "A":
                    //System.out.println("Picked A");
                    Camper newCamper = new Camper(true);
                    theList.set((newCamper.getRegistrationId() - 1), newCamper);
                    CamperFileUtility.writeCamperToRandomFile(raf, newCamper);

                    break;
                case "S":
                    System.out.println("Here are the campers");
                    for (Camper camper : theList) {
                        //Issue#1  BJM 20190923 Only showing valid campers
                        if (camper.getRegistrationId() > 0) {
                            System.out.println(camper);
                        }
                    }
                    break;
                case "X":
                    System.out.println("Goodbye");
                    break;
                default:
                    System.out.println("Invalid option");
                    break;

            }
        } while (!option.equalsIgnoreCase("x"));
    }

    public static RandomAccessFile setupRandomAccessFile(RandomAccessFile raf) throws IOException {
        //Only create and write 10000 spaces if the file does not already exist.
        Path path = Paths.get(FILE_NAME);

        if (Files.notExists(path)) {
            raf = new RandomAccessFile(FILE_NAME, "rw");
            String tenSpaces = "          ";
            for (int count = 1; count <= 1000; count++) {
                raf.writeBytes(tenSpaces);
            }
        } else {
            raf = new RandomAccessFile(FILE_NAME, "rw");
        }

        return raf;
    }

    /**
     * Demonstrate writing an object to file.
     *
     * @author BJM
     * @since 20190920
     */
    public static void testWriteObject() {
        //Adding sample code for reading/writing objects to file.
        //Testing writing/reading objects.
        //Write an object to file
        CamperFileUtility cfu = new CamperFileUtility((PATH + "campersRandom.ser"));
        Camper test = new Camper();
        test.setFirstName("BJ");
        cfu.WriteObjectToFile(test);

        Camper testBack = new Camper();
        testBack = (Camper) cfu.readObjectFromFile();
        Object testObject = testBack;

        System.out.println(testBack);
        System.out.println(testObject);

    }

    /**
     * This method will load the campers from the file at the program startup.
     *
     * @param campers
     * @since 20150917
     * @author BJ MacLean
     */
    public static void loadCampers(ArrayList campers, RandomAccessFile raf) throws IOException {
        System.out.println("Loading campers from random accessfile");
        int countOfCampers = 0;
        for (int count = 1; count <= 100; count++) {
            //St
            int startLocation = (count - 1) * 100;
            raf.seek(startLocation);
            byte[] camperBytes = new byte[100];
            raf.read(camperBytes);
            String camperJson = new String(camperBytes);

            camperJson = camperJson.trim();
            Camper temp;
            if (camperJson.length() > 0) {
                //Get a camper from the string
                Gson gson = new Gson();
                temp = gson.fromJson(camperJson, Camper.class);
            } else {
                //Create a new camper
                temp = new Camper();
            }
            campers.add(temp);
        }
        System.out.println("Finished...Loading campers from file (Loaded " + countOfCampers + " campers)\n\n");

    }

//This method no longer used with the random access approach.
///**
// * This method will load the campers from the file at the program startup.
// *
// * @param campers
// * @since 20150917
// * @author BJ MacLean
// */
//public static void loadCampers(ArrayList campers) {
//        System.out.println("Loading campers from file");
//        int count = 0;
//
//        try {
//            ArrayList<String> test = (ArrayList<String>) Files.readAllLines(Paths.get(FILE_NAME));
//
//            for (String current : test) {
//                System.out.println("Loading:  " + current);
//                //Get a camper from the string
//                //Camper temp = new Camper(current);
//                Camper temp = Camper.newInstance(current);
//                
//                campers.add(temp);
//                count++;
//            }
//
//        } catch (IOException ex) {
//            System.out.println("Error loading campers from file.");
//            System.out.println(ex.getMessage());
//        }
//
//        System.out.println("Finished...Loading campers from file (Loaded " + count + " campers)\n\n");
//
//    }
}
